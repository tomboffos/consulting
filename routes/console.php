<?php

use App\Services\Chat\ChatService;
use App\Services\Payments\ProfileService;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');


Artisan::command('check-profiles',function(){
    $service = new ProfileService();
    $service->checkProfilePayments();
});

Artisan::command('check-closed-chats',function(){
    $service = new ChatService();
    $service->checkConsultations();
});

Artisan::command('check-opened-chats',function (){
    $service = new ChatService();
    $service->checkPayedConsultations();
});

