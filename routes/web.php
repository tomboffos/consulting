<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('login');


Route::prefix('/consultation')->group(function(){
    Route::get('/success',function(){
        return view('welcome');
    })->name('consultation.success');
    Route::get('/fail',function(){
        return view('welcome');
    })->name('consultation.fail');
});


Route::prefix('/profile')->group(function(){
    Route::get('/success',function(){
        return view('welcome');
    })->name('profile.success');
    Route::get('/fail',function(){
        return view('welcome');
    })->name('profile.fail');
});
