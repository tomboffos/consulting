<?php

use App\Http\Controllers\Api\Auth\ConsultantController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Catalog\BannerController;
use App\Http\Controllers\Api\Catalog\CategoryController;
use App\Http\Controllers\Api\Catalog\ProfileController;
use App\Http\Controllers\Api\Chat\ChatController;
use App\Http\Controllers\Api\Chat\MessageController;
use App\Http\Controllers\Api\Consultant\ChatConsultantController;
use App\Http\Controllers\Api\Consultant\ConsultationController;
use App\Http\Controllers\Api\Consultant\ProfilePaymentController;
use App\Http\Controllers\Api\Consultant\ReviewController;
use App\Http\Controllers\Api\Payment\ConsultationOrderController;
use App\Http\Controllers\Api\User\CardController;
use App\Http\Controllers\Api\User\MainController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/accept/card', [CardController::class, 'accept'])->name('accept-card');
Route::any('/consultation/payment/status', [ConsultationOrderController::class, 'acceptPaymentStatus'])->name('consultation.status');
Route::any('/profile/payment/status', [\App\Http\Controllers\Api\Consultant\ProfileController::class, 'acceptPaymentStatus'])->name('profile.status');
Route::get('/profiles',[ProfileController::class,'rated']);
Route::prefix('/auth')->group(function () {
    Route::prefix('/register')->group(function () {
        Route::post('/', [RegisterController::class, 'register']);
        Route::post('/check', [RegisterController::class, 'registerCheck']);
    });

    Route::prefix('/login')->group(function () {
        Route::post('/', [LoginController::class, 'login']);
        Route::post('/check', [LoginController::class, 'loginCheck']);
    });
});

Route::get('/category/{category?}', [CategoryController::class, 'index']);
Route::resource('/banners', BannerController::class);
Route::prefix('/consultants')->group(function () {
    Route::post('/{category}', [ProfileController::class, 'index']);
    Route::get('/profiles/{profile}', [ProfileController::class, 'show']);
    Route::get('/free-time/{user}', [ProfileController::class, 'showFreeTime']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/user')->group(function () {
        Route::resource('/cards', CardController::class);
        Route::post('/card/main/{card}',[CardController::class,'update']);
        Route::resource('/reviews',ReviewController::class);
        Route::get('/', [MainController::class, 'user']);
        Route::post('/update', [MainController::class, 'update']);
        Route::resource('/chats', ChatController::class);
        Route::resource('/message', MessageController::class);
        Route::resource('/consultation', ConsultationOrderController::class);
    });


    Route::prefix('/consultant')->group(function () {
        Route::get('/categories',[CategoryController::class,'getAll']);
        Route::resource('/chats',ChatConsultantController::class);
        Route::resource('/profile', \App\Http\Controllers\Api\Consultant\ProfileController::class);
        Route::resource('/profile-payments',ProfilePaymentController::class);
        Route::get('/subscriptions',[\App\Http\Controllers\Api\Consultant\ProfileController::class,'subscriptions']);
        Route::get('/orders',[ConsultationController::class,'index']);
        Route::prefix('/consultation')->group(function(){
            Route::post('/accept/{order}',[ConsultationController::class,'acceptOrder']);
            Route::post('/decline/{order}',[ConsultationController::class,'declineOrder']);
        });
    });


    Route::post('/logout', [MainController::class, 'logout']);
});


