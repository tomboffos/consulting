<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FiltrationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->post('/api/consultants/6',[
            'times' => [
                '12:00','13:00'
            ]
        ]);

        $response->dump();

        $response->assertStatus(200);
    }
}
