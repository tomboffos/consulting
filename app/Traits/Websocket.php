<?php


namespace App\Traits;


use Illuminate\Support\Facades\Http;

class Websocket
{

    public static function sendEvent($event, $data)
    {
        Http::post('https://socket.bhub.kz',[
            'event' => $event,
            'data' => $data
        ]);
    }
}
