<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
        'name',
        'symbol',
        'code_name'
    ];

    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;
}
