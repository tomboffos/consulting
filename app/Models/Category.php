<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'name',
        'order',
        'image',
        'category_id'
    ];

    public function child()
    {
        return $this->hasMany(Category::class)->orderBy('order','asc');
    }

    public function profiles()
    {
        return $this->hasMany(Profile::class)->where('profile_status_id',1);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
