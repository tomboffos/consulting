<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens, SoftDeletes;

    public const CHAT_STATUSES = [
        2, 3, 4, 5
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'surname',
        'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sms_code()
    {
        return $this->hasOne(SmsCode::class);
    }

    public function rating()
    {
        if ($this->hasMany(Review::class, 'user_profile_id', 'id')->count() != 0)
            return $this->hasMany(Review::class, 'user_profile_id', 'id')->sum('rating')
                / $this->hasMany(Review::class, 'user_profile_id', 'id')->count();
        return $this->hasMany(Review::class, 'user_profile_id', 'id')->sum('rating');

    }

    public function chats()
    {
        if ($this->role_id == 2)
            return $this->hasMany(Chat::class, 'id', 'client');
        else
            return $this->hasMany(Chat::class, 'id', 'consultant');
    }

    public function main_card()
    {
        return $this->hasOne(Card::class)->where('is_main', 1)->first();
    }

    public function checkMainCard()
    {
        return $this->hasOne(Card::class)->where('is_main', 1)->exists();
    }

    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    public function profiles()
    {
        return $this->hasMany(Profile::class)->where('profile_status_id', 1);
    }

    public function orders()
    {
        return $this->hasMany(ConsultationOrder::class)
            ->orderBy('id', 'desc')->where('payment_status_id', 2)->paginate(10);
    }

    public function chatOffers()
    {
        return $this->hasMany(ConsultationOrder::class)
            ->whereIn('consultation_status_id', self::CHAT_STATUSES)->orderBy('id', 'desc')->get();
    }

    public function devices()
    {
        return $this->hasMany(UserDevice::class)->pluck('device_token');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'user_profile_id', 'id');
    }

    public function offers()
    {
        return $this->hasManyThrough(ConsultationOrder::class, Profile::class)
            ->where('payment_status_id', 2)
            ->where('consultation_status_id', 1);
    }

    public function readyOffers()
    {
        return $this->hasManyThrough(ConsultationOrder::class, Profile::class)
            ->whereIn('consultation_status_id', self::CHAT_STATUSES)->where('payment_status_id', 2);
    }

    public function consultationChatOffers()
    {
        return $this->hasManyThrough(ConsultationOrder::class, Profile::class)
            ->whereIn('consultation_status_id', self::CHAT_STATUSES)->orderBy('id', 'desc')->get();
    }

    public function profilePayments()
    {
        return $this->hasManyThrough(ProfilePayment::class, Profile::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Profile::class)->where('profile_status_id', 1)->get();
    }

}
