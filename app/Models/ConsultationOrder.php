<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultationOrder extends Model
{
    protected $fillable = [
        'user_id',
        'payment_status_id',
        'profile_id',
        'start_time',
        'end_time',
        'consultation_status_id',
        'price'
    ];

    use HasFactory,SoftDeletes;


    public function payment_status()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function consultation_status()
    {
        return $this->belongsTo(ConsultationStatus::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function chat()
    {
        return $this->hasMany(Chat::class);
    }

}
