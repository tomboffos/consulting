<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'user_id',
        'card_hash',
        'card_distributor',
        'expiration_date',
        'is_main',
        'card_id'
    ];
}
