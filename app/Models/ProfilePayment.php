<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfilePayment extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'profile_id',
        'payment_status_id',
        'end_time',
        'price',
        'currency_id',
    ];

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
