<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{

    protected $fillable = [

        'user_id',
        'sms_code'
    ];


    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
