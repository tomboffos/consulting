<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model
{
    protected $fillable = [
        'consultation_order_id',
        'client',
        'consultant'
    ];

    use HasFactory,SoftDeletes;

    public function messages()
    {
        return $this->hasMany(Message::class)->orderBy('id','desc')->get();
    }

    public function opposite_user($id)
    {
        if ($id == $this->client)
            return $this->belongsTo(User::class,'consultation_id','id')->first();
        return $this->belongsTo(User::class,'client','id')->first();
    }

}
