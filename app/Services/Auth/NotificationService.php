<?php


namespace App\Services\Auth;


use App\Models\SmsCode;
use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class NotificationService
{

    public function sendCode(User $user)
    {
        if ($user->sms_code) {
            $user->sms_code->update([
                'sms_code' => mt_rand(1111, 9999)
            ]);
            return $this->smsCode($user->sms_code);
        } else {
            $smsCode = SmsCode::create([
                'user_id' => $user->id,
                'sms_code' => mt_rand(1111, 9999)
            ]);
            return $this->smsCode($smsCode);
        }


    }

    public function smsCode(SmsCode $smsCode)
    {
        $sms_code_array = [
            'login' => 'IRONL',
            'psw' => 'Timatima02tima',
            'phones' => $smsCode->user->phone,
            'mes' => "Ваш код $smsCode->sms_code, GETSUB",
        ];

//        $data = http_build_query([
//            'receiver' => $user->phone,
//            'text' => "Ваш код $smsCode->code, BHUB",
//            'apiKey' => $this->token
//        ]);

        $response = Http::get('https://smsc.ru/sys/send.php?login='.$sms_code_array['login'].'&psw='.$sms_code_array['psw'].'&phones='.$sms_code_array['phones'].'&mes='.$sms_code_array['mes']);
//        Log::info('response  '.print_r($response->body()));
        if ($response->successful()) {
            return $smsCode;
        } else {
            return $smsCode;
        }

    }

    public function checkCode($code, SmsCode $smsCode)
    {
        return $smsCode->sms_code == $code;
    }

    public function registerDevice(string $device_token, User $user)
    {
        if (!UserDevice::where('device_token', $device_token)->where('user_id', $user->id)->exists())
            UserDevice::create([
                'device_token' => $device_token,
                'user_id' => $user->id
            ]);
        return true;
    }


    public function deleteDevice(string $device_token,User $user)
    {
        User::where('device_token',$device_token)->where('user_id',$user->id)->delete();
    }
}
