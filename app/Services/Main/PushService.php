<?php


namespace App\Services\Main;


use Illuminate\Support\Facades\Http;

class PushService
{
    private $serverKey = 'AAAAePpI8VA:APA91bEFQVdFR-h8k1Sxo8l29kVrqXTOxJepcx7dFRsY43YjxTv1lIu45P85XH6IXrQkWIkl8WPJAy4DiFp0HM5Qi9zchz2CIqZx7FU54KbbIvT048hLp1X4-1aD8SFMZ3bhvsWkddlP';

    public function send($device_tokens,string $title,string $text)
    {
        $response = Http::withHeaders([
            'Authorization' => "key=$this->serverKey",
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $device_tokens,
            'notification' => [
                'title' => $title,
                'body' => $text,
                "sound" => 'notification.wav',
            ],
        ]);

        return $response->successful();
    }


}
