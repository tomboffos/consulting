<?php


namespace App\Services\Chat;


use App\Models\Chat;
use App\Models\ConsultationOrder;
use App\Models\Message;
use App\Models\ProfilePayment;
use App\Services\Main\PushService;
use App\Traits\Websocket;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ChatService
{
    private $push_service;

    public function __construct()
    {
        $this->push_service = new PushService();
    }

    public function createChat(ConsultationOrder $order)
    {
        Chat::create([
            'consultation_order_id' => $order->id,
            'client' => $order->user->id,
            'consultant' => $order->profile->user->id
        ]);
    }


    public function sendMessage(Message $message)
    {
        $this->push_service->send(
            $message->chat->opposite_user($message->user_id)->devices(),
            $message->body,
            'Вам пришло сообщение'
        );

        Websocket::sendEvent('chat-message-' . $message->chat->id, $message);

        Websocket::sendEvent('user-chat-event-' . $message->chat->opposite_user($message->user_id)->id, $message);
    }


    public function checkConsultations()
    {
        $consultations = ConsultationOrder::where('payment_status_id', 2)
            ->where('consultation_status_id', 2)
            ->whereDate('end_time', Carbon::now())
            ->whereTime('end_time', Carbon::now()->hour . ':'.Carbon::now()->minute . ':0')->get();
        $this->consultationClose($consultations);

    }

    private function consultationClose($consultations)
    {
        foreach ($consultations as $consultation)
            $consultation->update([
                'consultation_status_id' => 3
            ]);
    }

    public function checkPayedConsultations()
    {
        $consultations = ConsultationOrder::where('payment_status_id', 2)
            ->where('consultation_status_id', 5)
            ->whereDate('start_time', Carbon::now())
            ->whereTime('start_time', Carbon::now()->hour . ':'.Carbon::now()->minute . ':0')->get();
        $this->consultationOpen($consultations);

    }

    private function consultationOpen($consultations)
    {
        Log::info('Consultations : '.$consultations);
        foreach ($consultations as $consultation)
            $consultation->update([
                'consultation_status_id' => 2
            ]);
    }
}
