<?php


namespace App\Services\Payments;


use App\Models\ConsultationOrder;
use App\Models\Profile;
use App\Models\ProfilePayment;
use App\Models\User;
use App\Traits\Paybox;
use Illuminate\Support\Str;

class PaymentService
{
    private $service;


    public function __construct()
    {
        $this->service = new Paybox();
    }

    public function checkForCard(User $user)
    {
        return $user->checkMainCard();
    }

    public function consultationPayment(ConsultationOrder $order)
    {
        $this->service->setQuery([
            'pg_amount' => $order->profile->price,
            'pg_order_id' => (string)$order->id,
            'pg_user_id' => (string)$order->user_id,
            'pg_description' => 'Консультация номер ' . $order->id,
            'pg_salt' => Str::random(10),
            'pg_result_url' => route('consultation.status'),
            'pg_success_url' => route('consultation.success'),
            'pg_failure_url' => route('consultation.fail'),
            'pg_success_url_method' => 'GET',
            'pg_failure_url_method' => 'GET'
        ]);
        return $this->service->paymentLink();
    }

    public function consultationCardPayment(ConsultationOrder $order)
    {
        $service = new Paybox();
        $service->setQuery([
            'pg_amount' =>  "5", //$order->profile->price,
            'pg_order_id' => (string)$order->id,
            'pg_user_id' => (string)$order->user_id,
            'pg_card_id' => $order->user->main_card()->card_id ,
            'pg_description' => 'Консультация номер ' . $order->id,
            'pg_salt' => Str::random(10),
            'pg_result_url' => route('consultation.status'),
            'pg_success_url' => route('consultation.success'),
            'pg_failure_url' => route('consultation.fail'),
            'pg_success_url_method' => 'GET',
            'pg_failure_url_method' => 'GET'
        ]);

        return $service->pay();
    }

    public function profilePayment(ProfilePayment $profilePayment)
    {

    }

    public function profileCardPayment(ProfilePayment $profilePayment)
    {
        $this->service->setQuery([
            'pg_amount' =>  5 ,//$profilePayment->price,
            'pg_order_id' => (string)$profilePayment->id,
            'pg_user_id' => (string)$profilePayment->profile->user_id,
            'pg_card_id' => $profilePayment->profile->user->main_card()->card_id ,
            'pg_description' => 'Оплата подписки консультанта с номером:' . $profilePayment->profile->id,
            'pg_salt' => Str::random(10),
            'pg_result_url' => route('profile.status'),
            'pg_success_url' => route('profile.success'),
            'pg_failure_url' => route('profile.fail'),
            'pg_success_url_method' => 'GET',
            'pg_failure_url_method' => 'GET'
        ]);

        return $this->service->pay();
    }

}
