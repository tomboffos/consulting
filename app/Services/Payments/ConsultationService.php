<?php


namespace App\Services\Payments;


use App\Http\Resources\ConsultationOrderResource;
use App\Models\ConsultationOrder;
use App\Models\Profile;
use App\Models\Review;
use App\Models\User;
use App\Services\Chat\ChatService;
use App\Services\Main\PushService;
use App\Traits\Websocket;
use http\Env\Request;

class ConsultationService extends PaymentService
{
    private $push_service;
    private $chat_service;

    public function __construct()
    {
        $this->push_service = new PushService();
        $this->chat_service = new ChatService();
    }

    public function setDefaultValues($data, User $user)
    {

        $data['payment_status_id'] = 1;
        $data['consultation_status_id'] = 1;
        $data['user_id'] = $user->id;
        $data['card_id'] = $user->main_card()->id;
        return $data;
    }

    public function sendNotificationAfterPayment(ConsultationOrder $order)
    {
        $this->push_service->send($order->profile->user->devices(),
            'Вам пришла заявка на консультацию',
            'Заявка на консультацию предмет: ' . $order->profile->category->name . ', от:' . $order->user->name);

        $this->push_service->send($order->user->devices(),
            'Уведомления по поводу оплаты',
            'Ваша консультация успешно оплачена');

        Websocket::sendEvent('profile-consultation-' . $order->profile->id, new ConsultationOrderResource($order));
    }

    public function sendAcceptedNotification(ConsultationOrder $order)
    {

        $this->chat_service->createChat($order);

        return $this->push_service->send($order->user->devices(),
            'Уведомления GETSUB',
            'Консультант ' . $order->profile->user->name . ' принял время',);
    }

    public function declineNotification(ConsultationOrder $order)
    {
        return $this->push_service->send($order->user->devices(),
            'Уведомления GETSUB',
            'Консультант ' . $order->profile->user->name . ' отклонил время',);
    }




    public function checkProfileExist(ConsultationOrder $order, User $user)
    {
        return $order->profile->user_id == $user->id;
    }

    public function consultationClosedNotification(ConsultationOrder $consultationOrder)
    {
        Websocket::sendEvent('chat-closed-' . $consultationOrder->user->id, $consultationOrder);
        Websocket::sendEvent('chat-closed-' . $consultationOrder->profile->user->id, $consultationOrder);

        $this->push_service->send($consultationOrder->user->devices(),
            'Чат номер ' . $consultationOrder->id . ' закрыт',
            'Уведомления по консультации номер ' . $consultationOrder->id);
        $this->push_service->send($consultationOrder->profile->user->devices(),
            'Чат номер ' . $consultationOrder->id . ' закрыт',
            'Уведомления по консультации номер ' . $consultationOrder->id);
    }

    public function consultationOpenNotification(ConsultationOrder $consultationOrder)
    {
        Websocket::sendEvent('chat-opened-' . $consultationOrder->user->id, $consultationOrder);
        Websocket::sendEvent('chat-opened-' . $consultationOrder->profile->user->id, $consultationOrder);
        $this->push_service->send($consultationOrder->user->devices(),
            'Чат номер ' . $consultationOrder->id . ' открыт',
            'Уведомления по консультации номер ' . $consultationOrder->id);
        $this->push_service->send($consultationOrder->profile->user->devices(),
            'Чат номер ' . $consultationOrder->id . ' открыт',
            'Уведомления по консультации номер ' . $consultationOrder->id);
    }




}
