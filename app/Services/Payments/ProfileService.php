<?php


namespace App\Services\Payments;


use App\Models\Profile;
use App\Models\ProfilePayment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProfileService extends PaymentService
{
    public $data;

    public function setDefaults($data, User $user)
    {
        $data['currency_id'] = 1;
        $data['profile_status_id'] = 1;
        $data['user_id'] = $user->id;
        $this->data = $data;
    }

    public function approvePayment(Profile $profile)
    {


        if ($profile->user->cardMain)
            $this->profileCardPayment(ProfilePayment::create([
                'profile_id' => $profile->id,
                'payment_status_id' => 2,
                'end_time' => Carbon::now()->addMonth(),
                'price' => 5,
                'currency_id' => 1,
            ]));
    }


    public function checkProfilePayments()
    {
        $profiles = ProfilePayment::where('payment_status_id',2)
            ->whereDate('end_time',Carbon::now())
            ->whereTime('end_time','>=',Carbon::now()->hour.':0'.':0')
            ->whereTime('end_time','<=',Carbon::now()->addHour()->hour.':0'.':0')->get();
        $this->subscriptionWork($profiles);
    }

    public function subscriptionWork($profilePayments)
    {
        foreach ($profilePayments as $payment)
            $payment->profile->update([
               'profile_status_id' => 2
            ]);
    }

    public function setDefaultPayment($data)
    {
        $data['end_time'] = Carbon::now()->addMonth();
        $data['currency_id'] = 1;
        $data['price'] = 4200;
        $data['payment_status_id'] = 1;

        return $data;
    }
}
