<?php

namespace App\Observers;

use App\Models\ConsultationOrder;
use App\Services\Payments\ConsultationService;
use Illuminate\Support\Facades\Log;

class ConsultationOrderObserver
{
    /**
     * Handle the ConsultationOrder "created" event.
     *
     * @param \App\Models\ConsultationOrder $consultationOrder
     * @return void
     */
    public function created(ConsultationOrder $consultationOrder)
    {
        //
    }

    /**
     * Handle the ConsultationOrder "updated" event.
     *
     * @param \App\Models\ConsultationOrder $consultationOrder
     * @return void
     */
    public function updated(ConsultationOrder $consultationOrder)
    {
        $order_service = new ConsultationService();
        //
        Log::info('consultation statuses '.$consultationOrder->getOriginal('consultation_status_id').' '.$consultationOrder->consultation_status_id);
        if ($consultationOrder->getOriginal('consultation_status_id') != $consultationOrder->consultation_status_id)
            switch ($consultationOrder->consultation_status_id) {
                case 5:
                    $order_service->sendAcceptedNotification($consultationOrder);
                    break;
                case 6:
                    $order_service->declineNotification($consultationOrder);
                    break;
                case 3:
                    $order_service->consultationClosedNotification($consultationOrder);
                    break;
                case 2:
                    $order_service->consultationOpenNotification($consultationOrder);
                    break;

            }

        if ($consultationOrder->getOriginal('payment_status_id') != $consultationOrder->payment_status_id)
            $order_service->sendNotificationAfterPayment($consultationOrder);;

    }

    /**
     * Handle the ConsultationOrder "deleted" event.
     *
     * @param \App\Models\ConsultationOrder $consultationOrder
     * @return void
     */
    public function deleted(ConsultationOrder $consultationOrder)
    {
        //
    }

    /**
     * Handle the ConsultationOrder "restored" event.
     *
     * @param \App\Models\ConsultationOrder $consultationOrder
     * @return void
     */
    public function restored(ConsultationOrder $consultationOrder)
    {
        //
    }

    /**
     * Handle the ConsultationOrder "force deleted" event.
     *
     * @param \App\Models\ConsultationOrder $consultationOrder
     * @return void
     */
    public function forceDeleted(ConsultationOrder $consultationOrder)
    {
        //
    }

}
