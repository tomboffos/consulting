<?php

namespace App\Providers;

use App\Models\ConsultationOrder;
use App\Models\Message;
use App\Observers\ConsultationOrderObserver;
use App\Observers\MessageObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        ConsultationOrder::observe(ConsultationOrderObserver::class);
        Message::observe(MessageObserver::class);
    }
}
