<?php

namespace App\Http\Controllers\Api\Chat;

use App\Http\Controllers\Controller;
use App\Http\Resources\ConsultationOrderWithChatResource;
use App\Http\Resources\User\ChatResource;
use App\Http\Resources\User\MessageResource;
use App\Models\Chat;
use App\Models\ConsultationOrder;
use App\Models\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    //


    public function index(Request $request)
    {
        return ConsultationOrderWithChatResource::collection($request->user()->chatOffers());
    }

    public function show(Chat $chat)
    {
        return MessageResource::collection($chat->messages());
    }

    public function destroy(Chat $id)
    {
        $id->delete();
        return response(['message' => 'Чат был удален']);
    }
}
