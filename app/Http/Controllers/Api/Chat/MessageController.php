<?php

namespace App\Http\Controllers\Api\Chat;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessageStoreRequest;
use App\Http\Resources\User\MessageResource;
use App\Models\Message;
use App\Services\Chat\ChatService;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    //
    /**
     * @var ChatService
     */
    private $chat_service;

    public function __construct()
    {
        $this->chat_service = new ChatService();
    }

    public function store(MessageStoreRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;

        $message = Message::create($data);


        return new MessageResource($message);
    }
}
