<?php

namespace App\Http\Controllers\Api\Consultant;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfilePaymentRequest;
use App\Http\Resources\Api\ProfilePaymentResource;
use App\Models\ProfilePayment;
use App\Services\Payments\ProfileService;
use Illuminate\Http\Request;

class ProfilePaymentController extends Controller
{
    //
    public function index(Request $request)
    {
        return ProfilePaymentResource::collection($request->user()->profilePayments);
    }

    public function store(ProfilePaymentRequest $profilePaymentRequest)
    {
        $payment = new ProfileService();

        if (!$payment->checkForCard($profilePaymentRequest->user())) return response([
            'message' => 'Привяжите карту и сделайте ее основной'
        ], 400);

        return $payment->profileCardPayment(ProfilePayment::create($payment->setDefaultPayment($profilePaymentRequest->validated())));
    }

}
