<?php

namespace App\Http\Controllers\Api\Consultant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Consultation\Profile\StoreRequest;
use App\Http\Requests\Consultation\Profile\UpdateRequest;
use App\Http\Resources\ProfileResource;
use App\Models\Category;
use App\Models\Profile;
use App\Models\ProfilePayment;
use App\Services\Payments\ProfileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    //
    private $service;

    public function __construct()
    {
        $this->service = new ProfileService();
    }

    public function index(Request $request)
    {
        return ProfileResource::collection($request->user()->profiles);
    }

    public function show(Profile $id)
    {
        return new ProfileResource($id);
    }


    public function update(Profile $id, UpdateRequest $request)
    {
        $data = $request->validated();

        $id->update($data);

        return new ProfileResource($id);
    }

    public function pay(Profile $id)
    {
        return $this->service->approvePayment($id);
    }

    public function store(StoreRequest $request)
    {

        $this->service->setDefaults($request->validated(), $request->user());


        return new ProfileResource(Profile::create($this->service->data));
    }

    public function delete(Profile $id)
    {
        $id->delete();

        return new ProfileResource($id);
    }

    public function acceptPaymentStatus(Request $request)
    {
//        Log::info('Result '.$request->all());
        if ($request->pg_result){
            ProfilePayment::find($request->pg_order_id)->profile->update([
                'profile_status_id' => 1
            ]);
            ProfilePayment::find($request->pg_order_id)->update([
                'payment_status_id' => 2
            ]);
        }

    }

    public function subscriptions(Request $request)
    {
        return ProfileResource::collection($request->user()->subscriptions());
    }

}
