<?php

namespace App\Http\Controllers\Api\Consultant;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewStoreRequest;
use App\Models\ConsultationOrder;
use App\Models\Profile;
use App\Models\Review;
use App\Services\Payments\ConsultationService;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    //
    private $service;

    public function __construct()
    {
        $this->service = new ConsultationService();
    }

    public function store(ReviewStoreRequest $request)
    {
        $data = $request->validated();

        if (Review::where([
            'user_id' => $request->user()->id,
            'user_profile_id' => $request->user_profile_id,
        ])->exists()) return response(['message' => 'Отзыв уже есть'],400);

        $data['user_id'] = $request->user()->id;

        return Review::create($data);
    }
}
