<?php

namespace App\Http\Controllers\Api\Consultant;

use App\Http\Controllers\Controller;
use App\Http\Resources\ConsultationOrderWithChatResource;
use App\Http\Resources\User\MessageResource;
use App\Models\Chat;
use Illuminate\Http\Request;

class ChatConsultantController extends Controller
{
    //

    public function index(Request $request)
    {
        return ConsultationOrderWithChatResource::collection($request->user()->consultationChatOffers());
    }

    public function show(Chat $id)
    {
        return MessageResource::collection($id->messages);
    }
}
