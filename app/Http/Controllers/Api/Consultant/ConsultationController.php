<?php

namespace App\Http\Controllers\Api\Consultant;

use App\Http\Controllers\Controller;
use App\Http\Resources\ConsultationOrderResource;
use App\Models\ConsultationOrder;
use App\Services\Payments\ConsultationService;
use Illuminate\Http\Request;

class ConsultationController extends Controller
{
    //
    private $order_service;

    public function __construct()
    {
        $this->order_service = new ConsultationService();
    }

    public function index(Request $request)
    {
        return ConsultationOrderResource::collection($request->user()->offers);
    }

    public function acceptOrder(ConsultationOrder $order, Request $request)
    {
        if ($this->order_service->checkProfileExist($order, $request->user()))
            return $order->update([
                'consultation_status_id' => 5
            ]);
        return response([
            'message' => 'Вы не являетесь консультантом этого заказа'
        ], 401);
    }

    public function declineOrder(ConsultationOrder $order, Request $request)
    {
        if ($this->order_service->checkProfileExist($order, $request->user()))
            return $order->update([
                'consultation_status_id' => 6
            ]);
        return response([
            'message' => 'Вы не являетесь консультантом этого заказа'
        ], 401);
    }
}
