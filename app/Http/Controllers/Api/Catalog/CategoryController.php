<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function getAll()
    {
        return CategoryResource::collection(Category::get());
    }

    public function index(Category $category,Request $request)
    {
        return CategoryResource::collection(Category::where(function($query) use ($category,$request){
            if ($category)
                $query->where('category_id',$category->id);
            else
                $query->where('category_id',null);
            if ($request->has('search'))
                $query->where('name','iLIKE','%'.$request->search.'%');
        })->orderBy('order','asc')->get());
    }
}
