<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Http\Resources\ProfileResource;
use App\Http\Resources\UserResource;
use App\Models\Category;
use App\Models\Profile;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //

    public function index(Category $category, Request $request)
    {

        return ProfileResource::collection(Profile::where('category_id', $category->id)->where(function ($query) use ($request) {
            if ($request->has('prices'))
                $query->whereBetween('price', $request->prices);

            if ($request->has('times')) {
                $query->where(function($secondQuery) use ($request) {
                    $secondQuery->whereHas('user.readyOffers', function ($thirdQuery) use ($request) {
                        $thirdQuery->whereNotBetween('start_time', $request->times);
                    })->orWhereDoesntHave('user.readyOffers');
                });
            }
        })->where('profile_status_id', 1)->orderBy('id', 'desc')->get()->filter(function ($profile) use ($request) {
            if ($request->has('ratings'))
                return $profile->user->rating == 5 || $profile->user->rating == 4;
            return true;
        }));
    }

    public function show(User $profile)
    {
        return new UserResource($profile);
    }

    public function rated()
    {
        return ProfileResource::collection(Profile::where('profile_status_id',1)->get()
            ->sortBy(function($profile,$key){
            return $profile->user->rating;
        }));
    }

}
