<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterCheckRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\Auth\SmsCodeResource;
use App\Http\Resources\UserResource;
use App\Models\SmsCode;
use App\Models\User;
use App\Services\Auth\NotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    //
    private $code_service;

    public function __construct()
    {
        $this->code_service = new NotificationService();
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();

        if (isset($data['password']))
            $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        $code = $this->code_service->sendCode($user);

        return response([
            'code' => new SmsCodeResource($code),
        ], 200);
    }

    public function registerCheck(RegisterCheckRequest $registerCheckRequest)
    {
        $code = SmsCode::find($registerCheckRequest->code_id);

        if ($this->code_service->checkCode($registerCheckRequest->code, $code)
            && $this->code_service->registerDevice($registerCheckRequest->device_token,$code->user))
            return response([
                'user' => new UserResource($code->user),
                'token' => $code->user->createToken(env('APP_NAME'))->plainTextToken
            ], 200);
        else
            return response([
               'message' => 'Код не верный'
            ],422);
    }
}
