<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginCheckRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\Auth\SmsCodeResource;
use App\Http\Resources\UserResource;
use App\Models\SmsCode;
use App\Models\User;
use App\Services\Auth\NotificationService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    //
    private $code_service;

    public function __construct()
    {
        $this->code_service = new NotificationService();
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();

        $code = $this->code_service->sendCode($user);

        return response([
            'code' => new SmsCodeResource($code)
        ], 200);
    }

    public function loginCheck(LoginCheckRequest $request)
    {
        $code = SmsCode::find($request->code_id);

        if ($this->code_service->checkCode($request->code, $code)
            && $this->code_service->registerDevice($request->device_token,$code->user))
            return response([
                'user' => new UserResource($code->user),
                'token' => $code->user->createToken(env('APP_NAME'))->plainTextToken
            ], 200);
        else
            return response([
                'message' => 'Код не верный'
            ],422);


    }
}
