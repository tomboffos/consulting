<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Payments\ConsultationOrderRequest;
use App\Http\Resources\ConsultationOrderResource;
use App\Models\ConsultationOrder;
use App\Services\Payments\ConsultationService;
use Illuminate\Http\Request;

class ConsultationOrderController extends Controller
{
    //
    private $order_service;

    public function __construct()
    {
        $this->order_service = new ConsultationService();
    }

    public function index(Request $request)
    {
        return ConsultationOrderResource::collection($request->user()->orders());
    }

    public function store(ConsultationOrderRequest $request)
    {
        $data = $request->validated();

        if (!$request->user()->checkMainCard())
            return response(['message' => 'Приявжите карту'], 400);

        $data = $this->order_service->setDefaultValues($data,$request->user());

        $order = ConsultationOrder::create($data);

        return $this->order_service->consultationCardPayment($order);
    }

    public function acceptPaymentStatus(Request $request)
    {
        if($request->pg_result){
            $order = ConsultationOrder::find($request->pg_order_id);
            $order->update([
                'payment_status_id' => 2
            ]);
        }
    }
}
