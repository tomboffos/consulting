<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\CardResource;
use App\Models\Card;
use App\Traits\Paybox;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CardController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new Paybox();
    }

    //
    public function index(Request $request)
    {
        return CardResource::collection($request->user()->cards);
    }

    public function store(Request $request)
    {
        $this->service->setQuery([
            'pg_user_id' => $request->user()->id,
            'pg_post_link' => route('accept-card'),
            'pg_back_link' => route('login'),
            'pg_salt' => Str::random(10)
        ]);

        $response = $this->service->addCard();

        $xml = (array)new \SimpleXMLElement($response->body());

        return response(['xml' => $xml], 200);

    }

    public function update(Card $card, Request $request)
    {
        foreach ($request->user()->cards as $card){
            $card->update([
                'is_main' => 0
            ]);
        }
        $card->update(['is_main' => 1]);
        return new CardResource($card);
    }

    public function accept(Request $request)
    {
        if ($request['pg_xml']) {
            $card = $this->service->saveCard($request->pg_xml);
            return response([$card], 200);
        } else
            return response([], 400);

    }
}
