<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //

    public function user(Request $request)
    {
        return new UserResource($request->user());
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response([],200);
    }

    public function update(UpdateUserRequest $request)
    {
        $data = $request->validated();

        $request->user()->update($data);

        return new UserResource($request->user());
    }
}
