<?php

namespace App\Http\Requests\Payments;

use Illuminate\Foundation\Http\FormRequest;

class ConsultationOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'profile_id' => 'required|exists:profiles,id',
            'start_time' => 'required|date',
            'end_time' => 'required|date|after:start_date',
            'price' => 'required|numeric'
        ];
    }
}
