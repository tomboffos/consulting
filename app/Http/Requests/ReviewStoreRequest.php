<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'profile_id' => 'required|exists:profiles,id',
            'rating' => 'required',
            'details' => 'nullable',
            'consultation_order_id' => 'required|exists:consultation_orders,id',
            'user_profile_id' => 'required|exists:users,id'
        ];
    }
}
