<?php

namespace App\Http\Requests\Consultation\Profile;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'description' => 'required',
            'start_date' => 'required|date',
            'price' => 'required|numeric',
            'category_id' => 'required|exists:categories,id'
        ];
    }
}
