<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ConsultationOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user,
            'profile' => new ProfileResource($this->profile),
            'consultation_status' => $this->consultation_status,
            'payment_status' => $this->payment_status,
            'created_at' => $this->created_at->format('d.m.y'),
            'price' => $this->price,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time

        ];
    }
}
