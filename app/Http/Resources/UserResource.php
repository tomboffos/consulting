<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'profiles' => ProfileByUserResource::collection($this->profiles),
            'avatar' => $this->avatar == null ? null : asset($this->avatar),
            'reviews' => $this->reviews,
            'rating' => $this->rating,
            'phone' => $this->phone,

        ];
    }
}
