<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('profile_statuses')->insert([
           'name' => 'Активно',
           'id' => 1
        ]);

        DB::table('profile_statuses')->insert([
            'name' => 'Отключено',
            'id' => 2
        ]);


    }
}
