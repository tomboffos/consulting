<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsultationStatusAdditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('consultation_statuses')->insert([
            'name' => 'Консультация принята консультантом',
            'id' => 5
        ]);

        DB::table('consultation_statuses')->insert([
            'name' => 'Консультация отклонена консультантом',
            'id' => 6
        ]);
    }
}
