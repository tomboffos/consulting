<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsultationStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('consultation_statuses')->insert([
            'name' => 'Новая заявка',
            'id' => 1
        ]);

        DB::table('consultation_statuses')->insert([
            'name' => 'Чат начался',
            'id' => 2
        ]);

        DB::table('consultation_statuses')->insert([
            'name' => 'Чат закончился',
            'id' => 3,
        ]);

        DB::table('consultation_statuses')->insert([
            'name' => 'Оставлена заявка',
            'id' => 4
        ]);
    }
}
