<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Profile;
use App\Models\ProfileStatus;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Profile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'user_id' => User::inRandomOrder()->first()->id ,
            'category_id' => Category::inRandomOrder()->first()->id,
            'description' => $this->faker->text,
            'price' => mt_rand(500,10000),
            'start_date' => Carbon::now(),
            'currency_id' => 1,
            'profile_status_id' => ProfileStatus::inRandomOrder()->first()->id,
        ];
    }
}
